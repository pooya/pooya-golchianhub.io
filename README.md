# My new website base on jekyll 

## Notable features:

* Compatible with Jekyll 3 and GitHub Pages.
* Responsive templates. Looks good on mobile, tablet, and desktop devices.
* Gracefully degrading in older browsers. Compatible with Internet Explorer 9+ and all modern browsers.
* Minimal embellishments and subtle animations.
* Optional large feature images for posts and pages.
* Support for Disqus Comments


See a [live version of My New Jekyll Theme](http://pooya-golchian.github.io)
