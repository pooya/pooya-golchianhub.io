---
layout: page
title: My Projects
excerpt: "Instructions on how to install and customize the Jekyll theme So Simple."
modified: 2016-01-19
image:
  feature: bg4.jpg
  <!--credit: OpenSource!-->
  <!--creditlink: -->
---
# My New Jekyll Theme (Personal Website)
This website has been developed by github jekyll (Blog-aware and FlatFile DB with markdown).
You can FORK this [repo](https://github.com/pooya-golchian/pooya-golchian.github.io) and develope this project.
[url](http://pooyagolchian.ir)

# AxPrint.com
Senior UI developer, Drupal developer, Linux Admin at [axprint.com](http://axprint.com). 5 Sep 2015 - NOW

## My project in axprint:
* **Build [blog.axprint.com](http://blog.axprint.com)**
* **Redesign [axprint.com](http://axprint.com)**
* **Build axprint campaign landing page**
    - [axprint.com/landing/loveday94/](http://axprint.com/landing/loveday94/)  
    - [axprint.com/landing/pishgaman/](http://axprint.com/landing/pishgaman/)
    - [axprint.com/landing/coldseason/](http://axprint.com/landing/coldseason/)

# Rayvarz Software Engineering Company
UI Developer, Drupal Developer [Rayvarz Software Engineering Company](http://rayvarz.com) - 1year


## My project in Rayvarz:
* **Build and design [Rayvarz BPMS Portal drupal base](http://bpms.rayvarz.com)**


# FlyFish 2D android game
Develope and design  2D android game, FlyFish released on IranApps [FlyFish Game on IranApp.com Application store](http://iranapps.ir/app/com.pooya.FlyFish)
